﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.JobApplication
{
    [ExcelSheetName("Domain")]
    public class DomainModel : Model
    {
        [ExcelColumn("Identifier")]
        public string Identifier { get; set; }

        [ExcelColumn("Description")]
        public string Description { get; set; }

        [ExcelColumn("Enrollment date")]
        public DateTime? EnrollmentDate { get; set; }

        [ExcelColumn("Central administrator")]
        public bool? CentralAdministrator { get; set; }

        [ExcelColumn("Access number")]
        public long? AppCodeAccessNumber { get; set; }

        [ExcelColumn("RTGS number")]
        public long? AppCodeRTGSNumber { get; set; }

    }


}