﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Testware.Utilities;

namespace Testware.JobApplication
{
    public enum ComparisonModeEnum
    {
        Strict = 1,
        Relaxed = 2,
        IgnoreCase = 4,
    }
    
    /// <summary>
    /// When comparing a instance of a model with the actual data from a form, then we need to be able to compare those models, to verify the equalness of the data contained.
    /// 
    /// In some cases, comparing the models needs to be very strict, like case needs to be the same, null values shall not be ignored and so on.
    /// 
    /// The cases it needs to be loose or relaxed, like having case ignored, when the comparison part contains null then ignore and dont comnpare.
    /// 
    /// </summary>
    public abstract class Model
    {
        public override int GetHashCode()
        {
            return GetHashCode(ComparisonModeEnum.Strict);
        }

        public int GetHashCode(ComparisonModeEnum comparisonMode)
        {
            return Cryptography.GetMD5Hash(GetHashTable(comparisonMode).Values.ToArray());
        }

        public int GetHashCode(ComparisonModeEnum comparisonMode, string[] properties)
        {
            return Cryptography.GetMD5Hash(GetHashTable(comparisonMode, properties).Values.ToArray());
        }
        public Dictionary<string, int> GetHashTable(ComparisonModeEnum comparisonMode)
        {
            return GetHashTable(comparisonMode, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comparisonMode"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public Dictionary<string, int> GetHashTable(ComparisonModeEnum comparisonMode, string[] properties)
        {
            IEnumerable<PropertyInfo> propertyList;

            if (properties == null)
            {
                propertyList = this.GetType().GetProperties();
            }
            else
            {
                propertyList = from pInfo in this.GetType().GetProperties()
                               where properties.Contains(pInfo.Name)
                               select pInfo;
            }

            Dictionary<string, int> hashTable = new Dictionary<string, int>();

            foreach (PropertyInfo pInfo in propertyList)
            {
                var value = pInfo.GetValue(this, null);
                if (value != null && (comparisonMode & ComparisonModeEnum.IgnoreCase) == ComparisonModeEnum.IgnoreCase && value.GetType() == typeof(string))
                {
                    value = (value as string).ToUpper();
                }

                if ((comparisonMode & ComparisonModeEnum.Strict) == ComparisonModeEnum.Strict)
                    hashTable.Add(pInfo.Name, value == null ? -1 : value.GetHashCode());
                else
                {
                    if (value != null)
                    {
                        hashTable.Add(pInfo.Name, value.GetHashCode());
                    }
                }
            }
            return hashTable;
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public bool Equals(object obj, ComparisonModeEnum comparinsonMode)
        {
            var mine = this.GetHashTable(comparinsonMode);
            return this.GetHashCode(comparinsonMode) == (obj as Model).GetHashCode(comparinsonMode, mine.Keys.ToArray());
        }


        /// <summary>
        /// Outputs all properties and their values as debugging details.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder retval = new StringBuilder();
            retval.AppendFormat("Name: {0}", this.GetType().Name);
            foreach (PropertyInfo pInfo in this.GetType().GetProperties())
            {
                retval.Append("\n");
                var value = pInfo.GetValue(this, null);
                retval.AppendFormat("\t{0}: [{1}]", pInfo.Name, value == null ? "null" : value.ToString());
            }
            retval.Append("\n");
            return retval.ToString();
        }

    }
}
